<footer class="m-3 p-3 bg-white rounded-lg shadow-lg dark:bg-gray-300 dark:text-blue-600">
    {{$slot}}
</footer>
